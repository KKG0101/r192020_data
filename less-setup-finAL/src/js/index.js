

(function () {
    if ($('header').length > 0) {
        var docElem = document.documentElement,
                $html = document.querySelector('html'),
                didScroll = false,
                changeHeaderOn = 50;
        function init() {
            window.addEventListener(
                    'scroll',
                    function (event) {
                        if (!didScroll) {
                            didScroll = true;
                            setTimeout(scrollPage, 5);
                        }
                    },
                    false
                    );
        }
        function scrollPage() {
            var sy = scrollY();
            if (sy >= changeHeaderOn) {
                classie.add($html, 'compressed');
            } else {
                classie.remove($html, 'compressed');
            }
            didScroll = false;
        }
        function scrollY() {
            return window.pageYOffset || docElem.scrollTop;
        }
        init();
    }
})();

$(function () {
    if (-1 != navigator.userAgent.indexOf('MSIE'))
        var detectIEregexp = /MSIE (\d+\.\d+);/;
    else
        var detectIEregexp = /Trident.*rv[ :]*(\d+\.\d+)/;
    if (detectIEregexp.test(navigator.userAgent)) {
        var ieversion = new Number(RegExp.$1);
        12 == ieversion ?
                jQuery('body').addClass('IE12') :
                11 == ieversion ?
                jQuery('body').addClass('IE11') :
                10 == ieversion ?
                jQuery('body').addClass('IE10') :
                9 == ieversion && jQuery('body').addClass('IE9');
    }
    //Search for keywords within the user agent string.  When a match is found, add a corresponding class to the html element and return.  (Inspired by http://stackoverflow.com/a/10137912/114558)
    function addUserAgentClass(keywords) {
        for (var i = 0; i < keywords.length; i++) {
            if (navigator.userAgent.indexOf(keywords[i]) != -1) {
                $('html').addClass(keywords[i].toLowerCase());
                return; //Once we find and process a matching keyword, return to prevent less "specific" classes from being added
            }
        }
    }
    addUserAgentClass(['Chrome', 'Firefox', 'MSIE', 'Safari', 'Opera', 'Mozilla']); //Browsers listed generally from most-specific to least-specific
    addUserAgentClass(['Android', 'iPhone', 'iPad', 'Linux', 'Mac', 'Windows']); //Platforms, also in order of specificity

    var homeSlider = $(".home-banner home-slide").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 1000,
        pauseOnHover: false,
        fade: true,
        accessibility: false,
        autoplaySpeed: 6000,
        arrows: false,
        slidesToShow: 1,
        draggable: true,
        centerMode: false,
        useCSS: true,
        slidesToScroll: 1,
        prevArrow: '<div class="prev slick-arrow"><a href="javascript:void(0)">PREVIOUS</a></div>',
        nextArrow: '<div class="next slick-arrow"><a href="javascript:void(0)">NEXT</a></div>',
        customPaging: function (slider, i) {
            return '<a href="javascript:void(0)">' + i + '</a>';
        }
    });

    var testimonialSlider = $(".testimonial-slider ul").slick({
        dots: false,
        infinite: true,
        autoplay: true,
        speed: 500,
        pauseOnHover: false,
        fade: false,
        accessibility: false,
        autoplaySpeed: 6000,
        arrows: true,
        slidesToShow: 1,
        draggable: true,
        centerMode: false,
        useCSS: true,
        slidesToScroll: 1,
        prevArrow: '<div class="prev slick-arrow"><a href="javascript:void(0)">PREVIOUS</a></div>',
        nextArrow: '<div class="next slick-arrow"><a href="javascript:void(0)">NEXT</a></div>',
        customPaging: function (slider, i) {
            return '<a href="javascript:void(0)">' + i + '</a>';
        }
    });

    $("[data-fancybox]").fancybox({
        animationEffect: "fade"
    });

  
    $('.tab-btns li').click(function () {
        var curIndex = $(this).index();
        $('.tab-btns li').removeClass('active');
        $(this).addClass('active');
        $('.tab-detail .content').hide();
        $('.tab-detail .content').eq(curIndex).fadeIn();
    });


    doneResizing();
    scrollContent();
    var resizeId;
    $(window).resize(function () {
        clearTimeout(resizeId);
        resizeId = setTimeout(doneResizing, 25);
    });
    $(window).bind('orientationchange', function () {
        doneResizing();
    });
});

function doneResizing() {
    isResizing = true;



    isResizing = false;
}

var browserMobile = false;
if ($('body').hasClass('layout-mobile'))
    browserMobile = true;
$(window).scroll(function () {
    scrollContent();
});



function scrollContent() {
    var $toLoad = $('.to-load');
    var $toLoadElement = $('.to-load-element');
    if (browserMobile)
        $('.to-load').addClass('loaded');
    $('.to-load-element').addClass('loaded');
    if (browserMobile) {
        newScroll = $(window).scrollTop();
    } else {
        if (window.scrollY > 0) {
            newScroll = window.scrollY;
        } else {
            newScroll = $('html,body').scrollTop();
        }
    }
    if (!browserMobile) {
        $toLoad.each(function () {
            var object = $(this);
            if (newScroll + $(window).height() * 1.05 > $(this).offset().top) {
                object.removeClass('no-anim');
                object.addClass('loaded');
            } else if (newScroll + $(window).height() < $(this).offset().top) {
                object.addClass('no-anim');
                object.removeClass('loaded');
            }
        });
        $toLoadElement.each(function () {
            var object = $(this);
            if (newScroll + $(window).height() * 1.05 > $(this).offset().top) {
                object.removeClass('no-anim');
                object.addClass('loaded');
            } else if (newScroll + $(window).height() < $(this).offset().top) {
                object.addClass('no-anim');
                object.removeClass('loaded');
            }
        });
    } else {
        $('.to-load').addClass('loaded');
        $('.to-load-element').addClass('loaded');
    }
    currentScroll = newScroll;
}

function whichBrs() {
    var agt = navigator.userAgent.toLowerCase();
    if (agt.indexOf('opera') != -1)
        return 'Opera';
    if (agt.indexOf('staroffice') != -1)
        return 'Star Office';
    if (agt.indexOf('webtv') != -1)
        return 'WebTV';
    if (agt.indexOf('beonex') != -1)
        return 'Beonex';
    if (agt.indexOf('chimera') != -1)
        return 'Chimera';
    if (agt.indexOf('netpositive') != -1)
        return 'NetPositive';
    if (agt.indexOf('phoenix') != -1)
        return 'Phoenix';
    if (agt.indexOf('firefox') != -1)
        return 'Firefox';
    if (agt.indexOf('chrome') != -1)
        return 'Chrome';
    if (agt.indexOf('safari') != -1)
        return 'Safari';
    if (agt.indexOf('skipstone') != -1)
        return 'SkipStone';
    if (agt.indexOf('msie') != -1)
        return 'Internet Explorer';
    if (agt.indexOf('netscape') != -1)
        return 'Netscape';
    if (agt.indexOf('mozilla/5.0') != -1)
        return 'Mozilla';
    if (agt.indexOf('/') != -1) {
        if (agt.substr(0, agt.indexOf('/')) != 'mozilla') {
            return navigator.userAgent.substr(0, agt.indexOf('/'));
        } else
            return 'Netscape';
    } else if (agt.indexOf(' ') != -1)
        return navigator.userAgent.substr(0, agt.indexOf(' '));
    else
        return navigator.userAgent;
}

$(window).load(function () {
    $(window).trigger('resize scroll');
    $('html').addClass('loaded');
    /*--- Site Loader---*/
    $('#main-wrap').animate({
        'opacity': '1'
    }, 500, function () {
        $('.pageLoader').fadeOut();
    });
});
 
$('#next').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('.carousel').carousel('next');
});

$('#prev').on('click', function (e) {
    e.preventDefault();
    e.stopPropagation();
    $('.carousel').carousel('prev');
});
  // story slider end

  

  // home banner slider 

  
$('.home-slide').slick({
    dots: true,
    arrows: true,
    autoplay: false,
     autoplaySpeed: 3000,
    infinite: false, 
    slidesToShow: 1,
    slidesToScroll: 1, 
    responsive: [
      { 
        breakpoint: 1300,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 1024,
        settings: {
         slidesToShow: 1,
         slidesToScroll: 1,
        }
      },
      {
        breakpoint: 665,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      } 
    ]
  });


  if ($('.carousel').length > 0){
     // debugger
    $('.carousel').carousel({
        padding: 90,
        shift: 55,
    });    
  }

 


  // filter toggle

 

  $('.filter-result-btn').click(function(){
    $(".filter-data-container").slideToggle();
    $(".filter-result-btn").toggleClass("show-data"); 
});



// faq accordion    

 

   // mobile dashboard tab js

   $(document).ready(function () {
    $(".tab-menu").click(function () {
        $(".tab-menu-m").fadeIn("3000").addClass("tab-menu-show");
    });
});

$(document).ready(function () {
    $(".tab-close-m").click(function () {
        $(".tab-menu-m").fadeOut("3000").removeClass("tab-menu-show");
    });
});




// $(function() {
//     $('select').selectric();
//   });


// menu js


$('.nav-toggle').click(function headd() {
   // console.log( "ready!" );
  //  alert('nav toggle click');
    if (!$('.nav-toggle').hasClass('active')) {
        $('.nav-toggle').addClass('active');
        $('html').addClass('before-menu-open');
        setTimeout(function () {
            $('html').addClass('menu-open');
        }, 30);
    } else {
        $('.nav-toggle').removeClass('active');
        $('html').removeClass('menu-open');
        setTimeout(function () {
            $('nav li').removeClass('show-menu-child');
            $('html').removeClass('before-menu-open sub-menu-opened');
        }, 400);
    }
}); 


 // img zoom on scroll
  
 $(window).scroll(function() {
	var scroll = $(window).scrollTop();
	$('.scroll-zoom').css({
		backgroundSize: (100 + scroll/5) + '%'
	});
});



// header js mobile


 
$('.sub-menu-icon').click(function(){ 
    $(".header-submenu").toggleClass("header-submenu-active"); 
    $(".sub-menu").toggleClass("submenu-active"); 
});

// login tab 
$('.login-tab-m .login-tab').on('click', function(){
    $(this).addClass('active-tb').siblings().removeClass('active-tb');  
}); 
 
$('.login-tab-m .login-tab').on('click', function () {
    var curIndex = $(this).index(); 
    $('.log-tb-c').hide();
    $('.log-tb-c').eq(curIndex).fadeIn();
});




if ($('.home-service-slide').length > 0){
    var windowWidth = $(window).width();   
    if(windowWidth < 769){
      //  $('.home-tab-container . dashboard-tab li:last-child').remove();
        $('.home-service-slide').slick({ 
            infinite: true,
            autoplay: false,
            speed: 1000, 
            autoplaySpeed: 1000,
            draggable: true, 
            arrows: true,
           centerMode: true,
             centerPadding: '0',
            slidesToShow: 5,
            responsive: [
              {
                breakpoint: 768,
                settings: {
                  arrows: true, 
                  slidesToShow: 2
                }
              },
              {
                breakpoint: 480,
                settings: {
                  arrows: true,
                //  centerMode: true,
                //  centerPadding: '40px',
                  slidesToShow: 1
                }
              }
            ]
          }); 
    }
    
    }



    // cart header

    $('.cart-tab').click(function(){ 
        $(".cart-tab").toggleClass("active"); 
        $(".cart-tab-mobile").toggleClass("cart-active"); 
    });



    // filter tab

    $('#choose-topic').click(function(){  
        $("#course").hide();  
        $("#sort").hide();  
        $("#topic").slideToggle();  
    });

    $('#type-course').click(function(){  
        $("#topic").hide();  
        $("#sort").hide();  
        $("#course").slideToggle();  
    });

    $('#sorting').click(function(){  
        $("#topic").hide();  
        $("#course").hide();  
        $("#sort").slideToggle();  
    });
 
    $('.filter-m-btn').click(function(){  
     $(".filter-mob-tab .filter-clear").toggleClass("hide"); 
     $(".tab-data-container").toggleClass("active"); 
     $(".clr-apply-filter").toggleClass("active"); 
        $(".filter-m").slideToggle("active");  
    });


    

    $('.m-topic').click(function(){   
        $(".content.course").removeClass("activer"); 
        $(".content.short").removeClass("activer");  
        $(".content.topic").addClass("activer"); 
    });

    $('.m-type').click(function(){  
        $(".content.topic").removeClass("activer");   
        $(".content.short").removeClass("activer");   
        $(".content.course").addClass("activer"); 
    });


    $('.m-sort').click(function(){  
        $(".content.course").removeClass("activer"); 
        $(".content.topic").removeClass("activer");  
        $(".content.short").addClass("activer"); 
    });
