$( document ).ready(function() {
  $('#petslide, #pethomeslide').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#pet').fadeIn();
  });
  $('#wfhslide, #wfhomeslide').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#wfh').fadeIn();
  });
  $('#fitnesslide, #fitnesslidepre').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#fitness').fadeIn();
  });
  $('#mentalslide, #mentalslide').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#mental').fadeIn();
  });
  $('#careerslide, #carrerprevslide').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#career').fadeIn();
  });
  $('#bookslide, #writebookprev').on('click', function(){
    $(this).parents().find('.slide-main-container').fadeOut();
    $('#book').fadeIn();
  });
});